const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const todoSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectID,
      ref: "User",
      require: true,
    },
    title: {
      type: String,
      required: true,
      trim: true,
    },
    description: {
      type: String,
      required: true,
      trim: true,
    },
    is_complete: {
      type: Boolean,
      default: false,
    },
    color: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

todoSchema.methods.isOwner = async function (user_id) {
  try {
    const todo = this;

    if (todo.user._id.toString() !== user_id.toString())
      throw new Error("You are not authorized to perform such action.");

    return true;
  } catch (error) {
    throw new Error(error);
  }
};

todoSchema.plugin(mongoosePaginate);
todoSchema.index({ title: "text", description: "text" });
const Todo = mongoose.model("Todo", todoSchema);

module.exports = Todo;
