require("dotenv").config();
require("./mongoose");

const express = require("express");
const cors = require("cors");

const { apiVersion } = require("./constants");
const { userRoutes, todoRoutes } = require("./routes");

const app = express();
const PORT = process.env.PORT || 3000;

// express middleware
app.use(cors());
app.use(express.json());

// api-end-points
app.use(apiVersion, userRoutes);
app.use(apiVersion, todoRoutes);

app.get("/", (req, res) => {
  res.send("Express is Up");
});

app.use("*", (req, res) => {
  return res.status(404).json({
    success: false,
    message: "API endpoint doesn't exist",
  });
});

app.listen(PORT, () => {
  `SERVER listening on port ${PORT}...!`;
});
