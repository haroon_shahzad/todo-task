const express = require("express");

const router = new express.Router();

const authMiddleware = require("../middleware/auth");
const {
  create,
  getTodoById,
  getAllTodo,
  updateTodo,
  deleteTodo,
  createDummyTodo,
} = require("../controllers/todo-controller");

router.post("/todo", authMiddleware, create);

router.get("/todo/:id", authMiddleware, getTodoById);

router.get("/all-todo", authMiddleware, getAllTodo);

router.patch("/todo", authMiddleware, updateTodo);

router.delete("/todo/:id", authMiddleware, deleteTodo);

router.get("/todo-dummy", authMiddleware, createDummyTodo);

module.exports = router;
