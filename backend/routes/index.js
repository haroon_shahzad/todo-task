const userRoutes = require("./user-routes");
const todoRoutes = require("./todo-routes");

module.exports = {
  userRoutes,
  todoRoutes,
};
