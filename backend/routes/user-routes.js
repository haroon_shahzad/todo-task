const express = require("express");
const authMiddleware = require("../middleware/auth");

const router = new express.Router();

const {
  register,
  login,
  getMe,
  logout,
} = require("../controllers/user-controller");

// register
router.post("/register", register);
// login
router.post("/login", login);
// me
router.get("/me", authMiddleware, getMe);
// logout
router.get("/logout", authMiddleware, logout);

module.exports = router;
