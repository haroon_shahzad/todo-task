const User = require("../model/User");
const { handleErrors } = require("../helpers");

module.exports = {
  async register(req, res) {
    try {
      const user = await new User(req.body);
      const token = await user.generateAuthToken();

      await user.save();

      res.status(200).send({
        user,
        token,
        msg: "Welcome to the Todo App.",
      });
    } catch (error) {
      handleErrors(res, error);
    }
  },
  async login(req, res) {
    try {
      const { email, password } = req.body;
      if (!email || !password)
        throw new Error("Email or Password is either missing or invalid.");

      const user = await User.findByCredentials({ email, password });
      const token = await user.generateAuthToken();

      res.status(201).send({
        user,
        token,
        msg: "Welcome Back to todo app.",
      });
    } catch (error) {
      handleErrors(res, error);
    }
  },
  async getMe(req, res) {
    try {
      res.status(200).send({ user: req.user });
    } catch (error) {
      handleErrors(res, error);
    }
  },
  async logout(req, res) {
    try {
      await req.user.logout();
      res.status(200).send({
        msg: "User logout Successfully",
      });
    } catch (error) {
      handleErrors(res, error);
    }
  },
};
