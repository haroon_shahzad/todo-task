const faker = require("faker");

const Todo = require("../model/Todo");
const { handleErrors } = require("../helpers");

module.exports = {
  async create(req, res) {
    try {
      const todo = await new Todo({
        ...req.body,
        user: req.user._id,
      });

      await todo.save();
      res.status(201).send({
        msg: "A new Todo successfully created.",
        todo,
      });
    } catch (error) {
      handleErrors(res, error);
    }
  },
  async getTodoById(req, res) {
    try {
      const { id } = req.params;
      const todo = await Todo.findById(id);
      if (!todo) res.status(404).send({ error: "404 not found" });
      await todo.isOwner(req.user._id);
      res.status(200).send({
        todo,
      });
    } catch (error) {
      handleErrors(res, error);
    }
  },
  async getAllTodo(req, res) {
    try {
      const { page = 1, limit = 30, search = "", type = "all" } = req.query;

      let is_complete;

      if (type === "all") {
        is_complete = [false, true];
      } else if (type === "true") {
        is_complete = true;
      } else {
        is_complete = false;
      }

      const todos = await Todo.paginate(
        {
          user: req.user._id,
          $or: [
            { title: new RegExp(search, "gi") },
            { description: new RegExp(search, "gi") },
          ],
          is_complete,
        },
        {
          page,
          limit,
          sort: { createdAt: -1 },
          customLabels: {
            docs: "todos",
          },
        }
      );

      res.status(200).send(todos);
    } catch (error) {
      handleErrors(res, error);
    }
  },
  async updateTodo(req, res) {
    try {
      const id = req.body.id;
      delete req.body.id;
      const canUpdate = ["title", "description", "is_complete", "color"];
      const fields = Object.keys(req.body);

      const validFields = fields.every((field) => canUpdate.includes(field));
      if (!validFields) throw new Error("Invalid Fields");

      const todo = await Todo.findById(id);
      if (!todo) res.status(404).send({ error: "404 not found" });
      await todo.isOwner(req.user._id);

      fields.forEach((field) => {
        todo[field] = req.body[field];
      });

      await todo.save();
      res.status(201).send({ todo, msg: "Todo updated successfully" });
    } catch (error) {
      handleErrors(res, error);
    }
  },
  async deleteTodo(req, res) {
    try {
      const todo = await Todo.findById(req.params.id);
      if (!todo)
        res
          .status(404)
          .send({ error: "Todo already deleted or not found...!!!" });
      await todo.isOwner(req.user._id);

      await todo.remove();

      res.status(201).send({
        msg: "Todo Deleted Successfully...!",
      });
    } catch (error) {
      handleErrors(res, error);
    }
  },
  async createDummyTodo(req, res) {
    try {
      let todoArr = [];

      for (let i = 0; i < 50; i++) {
        const title = faker.vehicle.model();
        const description = faker.lorem.paragraphs(2);
        const is_complete = faker.datatype.boolean();
        const color = faker.internet.color();

        todoArr.push({
          title,
          description,
          is_complete,
          color,
          user: req.user._id,
        });
      }

      const todos = await Todo.insertMany(todoArr);

      res.status(201).send({ todos });
    } catch (error) {
      handleErrors(res, error);
    }
  },
};
