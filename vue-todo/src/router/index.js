import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store/index";

import Home from "../views/Home.vue";
import AuthenticatedLayout from "../layout/authenticated-layout";
import AuthLayout from "../layout/auth-layout";

Vue.use(VueRouter);

const routes = [
  {
    path: "",
    component: AuthLayout,
    children: [
      {
        path: "/",
        name: "Home",
        component: Home,
      },
      {
        path: "/login",
        name: "Login",
        component: () => import("../views/auth/Login"),
        meta: {
          requiresAuth: false,
        },
      },
      {
        path: "/register",
        name: "Register",
        component: () => import("../views/auth/Register"),
        meta: {
          requiresAuth: false,
        },
      },
    ],
  },

  // layout for the authenticated users only
  {
    path: "",
    component: AuthenticatedLayout,
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: "/todo-list",
        name: "TodoList",
        component: () => import("../views/todo/TodoList"),
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: "/create-todo",
        name: "CreateTodo",
        component: () => import("../views/todo/CreateTodo"),
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: "/todo/:id",
        name: "Todo",
        component: () => import("../views/todo/Todo"),
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: "/edit-todo/:id",
        name: "EditTodo",
        component: () => import("../views/todo/EditTodo"),
        meta: {
          requiresAuth: true,
        },
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => {
  const { requiresAuth } = to.meta;
  let isAuthenticated = false;

  const token = localStorage.getItem("token");

  if (token) {
    const { user } = store.state.user;
    if (!user) {
      try {
        await store.dispatch("user/FETCH_USER");
        isAuthenticated = true;
      } catch (error) {
        console.log("{ROUTER INDEX}", error);
      }
    } else {
      isAuthenticated = true;
    }
  }

  if (requiresAuth && isAuthenticated) {
    next();
  } else if (requiresAuth && !isAuthenticated) {
    next("/login");
  } else if (!requiresAuth && isAuthenticated) {
    next("/todo-list");
  } else {
    next();
  }
});

export default router;
