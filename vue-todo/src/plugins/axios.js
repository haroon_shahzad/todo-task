import Vue from "vue";
import axios from "axios";

import { API_END_POINT } from "@/config";

const service = axios.create({
  baseURL: API_END_POINT,
});

service.interceptors.request.use(
  (config) => {
    // Do something before request is sent
    const token = localStorage.getItem("token");

    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }

    return config;
  },
  (err) => {
    Vue.notify({
      group: "backend",
      title: "Error",
      text: err?.response?.data?.error ?? "Something Went Wrong...!!!",
      type: "error",
    });
    return Promise.reject(err);
  }
);

// Add a response interceptor
service.interceptors.response.use(
  (response) => {
    const { token } = response.data;
    if (token) localStorage.setItem("token", token);

    if (response.data.msg) {
      Vue.notify({
        group: "backend",
        title: "Success",
        text: response?.data?.msg,
        type: "teal",
      });
    }

    return response;
  },
  (err) => {
    const { status } = err.response;
    if (status === 401 || status === 403) {
      localStorage.removeItem("token");
    }

    Vue.notify({
      group: "backend",
      title: "Error",
      text: err?.response?.data?.error ?? "Something Went Wrong...!!!",
      type: "error",
    });
    return Promise.reject(err);
  }
);

export default service;
