import Vue from "vue";

Vue.filter("truncate", (val, limit = 100) => {
  if (val && val.length > limit) return val.substr(0, limit) + " ...";
  else return val;
});
