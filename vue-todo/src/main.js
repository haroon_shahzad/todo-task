import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// plugins
import vuetify from "./plugins/vuetify";
import "./plugins/vue-notification";
import "./plugins/global-components";

// filters
import "./filters";

// scss
import "./scss/main.scss";

import axios from "./plugins/axios";
Vue.$axios = axios;

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
