export const todoColors = [
  {
    text: "Grey",
    value: "accent",
  },
  {
    text: "Grey Dark 1",
    value: "#455A64",
  },
  {
    text: "Grey Dark 2",
    value: "#37474F",
  },
  {
    text: "Grey 2",
    value: "#385F73",
  },
  {
    text: "Yellow",
    value: "warning",
  },
  {
    text: "Pink",
    value: "#C2185B",
  },
  {
    text: "Purple",
    value: "#8E24AA",
  },
  {
    text: "Deep Purple",
    value: "#4527A0",
  },
  {
    text: "Cyan",
    value: "#00838F",
  },
  {
    text: "Teal",
    value: "#00695C",
  },
];
