import axios from "../plugins/axios";

const defaultPagination = () => ({
  limit: 20,
  totalPages: 1,
  page: 1,
  pagingCounter: 1,
  hasPrevPage: false,
  hasNextPage: false,
  prevPage: null,
  nextPage: null,
});

export default {
  namespaced: true,
  state: {
    todos: [],
    search: "",
    pagination: defaultPagination(),
    type: "all",
    // ------
    todo: null,
    loading: false,
  },
  getters: {},
  mutations: {
    SET_TODOS(state, payload) {
      state.todos = [...state.todos, ...payload];
    },
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
    SET_TODO(state, payload) {
      state.todo = payload;
    },
    SET_PAGINATION(state, payload) {
      state.pagination = payload;
    },
    SET_NEXT_PAGE(state, payload) {
      state.nextPage = payload;
    },
    SET_SEARCH(state, payload) {
      state.loading = true;
      state.pagination = defaultPagination();
      state.todos = [];
      state.search = payload ?? "";
    },
    SET_TYPE(state, payload) {
      state.loading = true;
      state.pagination = defaultPagination();
      state.todos = [];
      state.type = payload;
    },
    SET_CLEAR_ALL(state) {
      state.todos = [];
      state.search = "";
      state.type = "all";
      state.pagination = defaultPagination();
    },
  },
  actions: {
    async FETCH_TODOS({ commit, state }, page) {
      try {
        const { search, type } = state;

        commit("SET_LOADING", true);
        const resp = await axios.get(
          `all-todo?page=${page}&search=${search}&type=${type}`
        );
        const { todos } = resp.data;

        commit("SET_TODOS", todos);
        delete resp.data.todos;
        commit("SET_PAGINATION", resp.data);
        commit("SET_LOADING", false);
      } catch (error) {
        console.log(error);
        commit("SET_LOADING", false);
      }
    },
    async CREATE_TODO({ commit }, todoData) {
      try {
        commit("SET_LOADING", true);
        const resp = await axios.post("todo", todoData);
        console.log(resp);
        commit("SET_LOADING", false);
        return true;
      } catch (error) {
        console.log(error);
        commit("SET_LOADING", false);
        throw new Error(error);
      }
    },
    async FETCH_TODO({ commit }, id) {
      try {
        commit("SET_LOADING", true);
        const resp = await axios.get(`todo/${id}`);
        const { todo } = resp.data;
        commit("SET_TODO", todo);
        commit("SET_LOADING", false);
      } catch (error) {
        console.log(error);
        commit("SET_LOADING", false);
      }
    },
    async DELETE_TODO({ commit }, id) {
      try {
        commit("SET_LOADING", true);
        await axios.delete(`todo/${id}`);
        commit("SET_LOADING", false);
        return true;
      } catch (error) {
        console.log(error);
        commit("SET_LOADING", false);
      }
    },
    async UPDATE_TODO({ commit }, todoData) {
      try {
        commit("SET_LOADING", true);
        await axios.patch(`todo`, todoData);
        commit("SET_LOADING", false);
        return true;
      } catch (error) {
        console.log(error);
        commit("SET_LOADING", false);
      }
    },
    async CREATE_DUMMY_TODOS({ commit, state }) {
      try {
        commit("SET_LOADING", true);
        const resp = await axios.get("todo-dummy");

        commit("SET_TODOS", [...state.todos, ...resp.data.todos]);
        commit("SET_LOADING", false);
        return true;
      } catch (error) {
        console.log(error);
        commit("SET_LOADING", false);
      }
    },
  },
};
