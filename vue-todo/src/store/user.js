import axios from "../plugins/axios";

export default {
  namespaced: true,
  state: {
    user: null,
    loading: false,
  },
  getters: {},
  mutations: {
    SET_USER(state, user) {
      state.user = user;
    },
    SET_LOADING(state, loading) {
      state.loading = loading;
    },
  },
  actions: {
    async FETCH_USER({ commit, rootState }) {
      try {
        rootState.appLoading = true;
        const resp = await axios.get("me");
        const { user } = resp.data;
        commit("SET_USER", user);
        rootState.appLoading = false;
        return true;
      } catch (error) {
        console.log(error);
        rootState.appLoading = false;
        throw new Error(error);
      }
    },
    async REGISTER_USER({ commit }, userData) {
      try {
        commit("SET_LOADING", true);
        const resp = await axios.post("register", userData);
        const { user } = resp.data;
        commit("SET_USER", user);
        commit("SET_LOADING", false);
        return true;
      } catch (error) {
        console.log(error);
        commit("SET_LOADING", false);
        throw new Error(error);
      }
    },
    async LOGIN_USER({ commit }, userData) {
      try {
        commit("SET_LOADING", true);
        const resp = await axios.post("login", userData);
        const { user } = resp.data;
        commit("SET_USER", user);
        commit("SET_LOADING", false);
        return true;
      } catch (error) {
        console.log(error);
        commit("SET_LOADING", false);
        throw new Error(error);
      }
    },
    async LOGOUT({ commit, rootState }) {
      try {
        rootState.appLoading = true;
        await axios.get("logout");
        commit("SET_USER", null);
        localStorage.removeItem("token");
        rootState.appLoading = false;
        return true;
      } catch (error) {
        console.log(error);
        rootState.appLoading = false;
        throw new Error(error);
      }
    },
  },
};
