import Vue from "vue";
import Vuex from "vuex";

import todo from "./todos";
import user from "./user";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    appLoading: false,
  },
  mutations: {
    SET_APP_LOADING(state, payload) {
      state.appLoading = payload;
    },
  },
  actions: {},
  modules: {
    todo,
    user,
  },
});
